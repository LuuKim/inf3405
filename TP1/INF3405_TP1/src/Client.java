import java.io.*;
import java.util.*;
import java.net.*;

public class Client {
	
	private static Socket socket;
	private static String clientFolder = "./src/client/uploads/";
	
	public static void main(String[] args) throws Exception 
	{
		String serverAdress = "127.0.0.1";
		int port = 5000;
		
		String IPaddress = "";
		boolean isIPValid = false;
		
		System.out.print("Entrez l'adresse IP du serveur : ");
		do {
			Scanner in = new Scanner(System.in);
			IPaddress = in.nextLine();
			if(checkIP(IPaddress)) {
				isIPValid = true;
				serverAdress = IPaddress;
			} else {
				isIPValid = false;
				System.out.println(IPaddress + " est une IP invalide." + "\n" + "Veuillez entrer une IP valide sur 4 octets separes par un '.' : ");
			}
		} while(!isIPValid);
		
		System.out.print("Entrez le port d'ecoute : ");
		String serverPort = "";
		boolean isPortValid = false;
		do {
			Scanner in = new Scanner(System.in);
			serverPort = in.nextLine();
			if(isNumeric(serverPort)) {
				if(checkPort(Integer.parseInt(serverPort))) {
					System.out.println("Connexion avec le port " + Integer.parseInt(serverPort));
					isPortValid = true;
				} else {
					System.out.println(serverPort + " est un port invalide, veuillez entrer un port valide : ");
					isPortValid = false;
				}
			} else {
				System.out.println(serverPort + " est un port invalide, veuillez entrer un port valide (ex: 5000) : ");
				isPortValid = false;
			}
			if(isPortValid) {
				port = Integer.parseInt(serverPort);
			}
		} while(!isPortValid);
		
		
		try {
			socket = new Socket(serverAdress, port);
			System.out.format("The server is running on %s:%d%n" , serverAdress, port);
			
			ObjectInputStream inObject = new ObjectInputStream(socket.getInputStream());
			
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			
			File clientRoot = new File(clientFolder);
			
			if(!clientRoot.isDirectory()) {
				clientRoot.mkdirs();
			}
			
			boolean exit = false;
			
			byte[] bytesArray = null;
			
			while(!exit) {
				DataInputStream in = new DataInputStream(socket.getInputStream());
				
				System.out.print("$ ");
				Scanner input = new Scanner(System.in);
				String command = input.nextLine();
				out.writeUTF(command);
				args = command.split(" ");
				switch (args[0]) {
					case "exit" :
						exit = true;
						System.out.println(in.readUTF());
						break;
					case "mkdir" :
						System.out.println(in.readUTF());
						break;
					case "cd" :
						System.out.println((String)in.readUTF());
						break;
					case "ls" :
						@SuppressWarnings("unchecked") List<String> list = (List<String>) inObject.readObject();
						for(int i = 0; i < list.size(); i++) {
							System.out.println(list.get(i));
						}
						break;
						
					case "upload" :

						
						File fileToSend = new File(clientFolder + args[1]);
						out.writeBoolean(fileToSend.exists());
						if(!fileToSend.exists()) {
							System.out.println("Le fichier " + args[1] + " n'existe pas.");
						} else {
							bytesArray = new byte[(int)fileToSend.length()];
							FileInputStream fis = new FileInputStream(fileToSend);
							BufferedInputStream bis = new BufferedInputStream(fis);
							in = new DataInputStream(bis);
							in.readFully(bytesArray, 0, bytesArray.length);
							
							out.writeLong(bytesArray.length);
							out.write(bytesArray, 0, bytesArray.length);
							out.flush();
							System.out.println("Le fichier " + args[1] + " a bien ete televerse.");
						}
										
						break;
						
					case "download" :
						boolean exists = (boolean)inObject.readObject();
						if(!exists) {
							System.out.println("Le fichier " + args[1] + " n'existe pas.");							
						} 
						else {
							bytesArray = new byte[1024];
							int bytesRead;
							int size = (int)inObject.readObject();
							OutputStream os = new FileOutputStream(clientFolder + args[1] );
							while (size > 0 && (bytesRead = inObject.read(bytesArray, 0, (int)Math.min(bytesArray.length, size))) != 1) {
								os.write(bytesArray, 0, bytesRead);
								size -= bytesRead;
							}
							System.out.println(in.readUTF());
							os.close();
						}

						break;
						
					default:
						System.out.println("Commande non reconnue");
						break;
				}				
			}
		 } finally {
			socket.close();
		}
	
	}
	
	/**
	 * verification du port entre 5000 et 5050
	 * @param port
	 * @return boolean
	 */
	private static boolean checkPort(int port) 
	{
		return port >= 5000 && port <= 5050;
	}
	
	/**
	 * verification de si une chaine de carateres est seulement composee de numeros (digits)
	 * @param s
	 * @return boolean
	 */
	public static boolean isNumeric(String s) {
		return s.matches("[0-9]+");
	}
	
	
	/**
	 * verification de si s est une adresse IP valide
	 * @param s
	 * @return boolean
	 */
	public static boolean checkIP(String s) {
		return s.matches("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
	}
	

}
