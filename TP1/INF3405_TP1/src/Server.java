import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class Server {	
	
	private static ServerSocket listener;
	private static String IPServer = "127.0.0.1";
	
	public static void main(String[] args) throws Exception {
		
		int clientNumber = 0;
		
		String serverAddress = IPServer;
		String serverPort = "5000";
		
		System.out.print("Entrez le port d'ecoute : ");
		boolean isPortValid = false;
		do {
			Scanner in = new Scanner(System.in);
			serverPort = in.nextLine();
			if(isNumeric(serverPort)) {
				if(checkPort(Integer.parseInt(serverPort))) {
					System.out.println("Connexion avec le port " + Integer.parseInt(serverPort));
					isPortValid = true;
					in.close();
				} else {
					System.out.println(serverPort + " est un port invalide, veuillez entrer un port valide : ");
					isPortValid = false;
				}
			} else {
				System.out.println(serverPort + " est un port invalide, veuillez entrer un port valide (ex: 5000) : ");
				isPortValid = false;
			}
				
		} while(!isPortValid);
		
		listener = new ServerSocket();
		listener.setReuseAddress(true);
		InetAddress serverIP =  InetAddress.getByName(serverAddress);
		
		listener.bind(new InetSocketAddress(serverIP, Integer.parseInt(serverPort)));
		
		System.out.format("The server is running on %s:%s%n", serverAddress, serverPort);
		
		try {
			while(true) {
				new ClientHandler(listener.accept(), clientNumber++).start();
			}
		}
		finally {
			listener.close();
		}
			
	}
	
	/**
	 * verification du port entre 5000 et 5050
	 * @param port
	 * @return boolean
	 */
	private static boolean checkPort(int port) 
	{
		return port >= 5000 && port <= 5050;
	}
	
	/**
	 * verification de si une chaine de carateres est seulement composee de numeros (digits)
	 * @param s
	 * @return boolean
	 */
	public static boolean isNumeric(String s) {
		return s.matches("[0-9]+");
	}
	
	/**
	 * Retourne la date et l'heure actuelle
	 * @param none
	 * @return String
	 */
	public static String getCurrentDate () {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd@HH:mm:ss");
		Date dateobj = new Date();
		return df.format(dateobj);
	}
	
	private static class ClientHandler extends Thread {
		private Socket socket;
		private int clientNumber;
		
		private static String rootPath = "./src/server/cloud/";
		private static String currentPath = rootPath;
		
		public ClientHandler(Socket socket, int clientNumber) {
			this.socket = socket;
			this.clientNumber = clientNumber;

		}
	
		public void run() {
			try {
				ObjectOutputStream outObject = new ObjectOutputStream(socket.getOutputStream());
				boolean exit = false;
				byte[] bytesArray = null;
								
				while(!exit) {
					DataOutputStream out = new DataOutputStream(socket.getOutputStream());
					DataInputStream in = new DataInputStream(socket.getInputStream());
					
					String command = in.readUTF();
					System.out.println('[' + IPServer + '.' + clientNumber + ':' + socket.getLocalPort() + '-' + getCurrentDate() + "]: " + command);
					String args[] = command.split(" ");
					switch (args[0]) {
					
						case "exit" :
							exit = true;
							out.writeUTF("Vous avez ete deconnecte avec succes.");
							break;
							
						case "mkdir" :
							if(currentPath.charAt(currentPath.length()-1) != '/') {
								currentPath += '/';
							}
							File folder = new File(currentPath + args[1]);
							folder.mkdirs();
							currentPath = currentPath.substring(0, currentPath.length() - 1);
							out.writeUTF("Le dossier " +  args[1] + " a ete cree.");
							break;
							
						case "cd" :
							if (args.length > 1 && !args[1].contains("..") && !args[1].contains("//")) {
								//cd /[dossier]
								if(args[1].charAt(0) == '/' && args[1].length() > 1) {
									args[1] = args[1].substring(1);
								}
								
								//cd [dossier]/
								if(args[1].length() > 1 && args[1].charAt(args[1].length() - 1) == '/') {
									args[1] = args[1].substring(0, args[1].length() - 1);
								}
							}
							
							//cd or cd ~
							if(args.length == 1 || (args.length == 2 && args[1].length() == 1 && args[1].equals("~"))) {
								out.writeUTF("Vous etes dans le meme repertoire (" + currentPath + ')');
							}
							
							//cd /
							else if(args.length == 2 && args[1].length() == 1 && args[1].equals("/")) {
								currentPath = rootPath;
								out.writeUTF("Vous etes a la racine du nuage.");
							} 
							
							//cd [dossier]
							else if (directoryExists(args[1]) && !args[1].equals("..") && !args[1].equals(".") && !areMultipleDirectories(args[1])) {
								currentPath += args[1];
								out.writeUTF("Vous etes dans le dossier " + args[1] + '.');
							}
							
							//cd [dossier/dossier/...]
							else if (areMultipleDirectories(args[1])) {
								String[] directories = args[1].split("/");
								String oldPath = currentPath;
								boolean pathCorrect = true;
								for(int i = 0; i < directories.length; i++) {
									if(directoryExists(directories[i])) {
										currentPath += directories[i];
									} else {
										pathCorrect = false;
										currentPath = oldPath;
										out.writeUTF("Repertoire introuvable");
										break;
									}
								}
								if(pathCorrect) {
									currentPath = oldPath + args[1];
									out.writeUTF("Vous etes dans le dossier " + directories[directories.length-1] + '.');
								}
								break;
							}
							
							//cd ..
							else if(args[1].length() == 2 && args[1].equals("..")) {
								if(currentPath.equals(rootPath)) {
									out.writeUTF("Vous etes a la racine du nuage, impossible d'acceder au repertoire parent.");
									break;
								} else {
									String[] directories = currentPath.split("/");
									currentPath = "";
									for(int i = 0; i < directories.length-1; i++) {
										if(i == 0) {
											currentPath += directories[i];
										} else {
											currentPath += '/' + directories[i];
										}
									}
									out.writeUTF("Vous etes dans le dossier " + directories[directories.length-2] + '.');
									String temp = rootPath.substring(0, rootPath.length() - 1);
									if(currentPath.equals(temp)) {
										currentPath += '/';
									}
								}
								break;
							}
							
							//inconnu
							else {
								out.writeUTF("Repertoire introuvable");
							}
							break;
							
						case "ls" :
							List<String> list = new ArrayList<String>();
							File directory = new File(currentPath);
							File[] files = directory.listFiles();
							for(int i = 0; i < files.length; i++) {
								if(files[i].isDirectory()) {
									list.add("[Folder] " + files[i].getName());
								} else {
									list.add("[File] " + files[i].getName());
								}
							}
							outObject.writeObject(list);
							outObject.flush();
							break;
							
						case "upload" :
							if(in.readBoolean()) {
								OutputStream ops = new FileOutputStream(currentPath + '/' + args[1]);
								long size = in.readLong();
								int bytesRead;
								bytesArray = new byte[1024];
								while(size > 0 && (bytesRead = in.read(bytesArray, 0, (int)Math.min(bytesArray.length, size))) != -1)
								{
									ops.write(bytesArray, 0, bytesRead);
									size -= bytesRead;
								}
								ops.close();
							}
							
							break;
							
						case "download" :
							File fileToSend = new File(currentPath +  '/' + args[1]);
							if(!fileToSend.exists()) {
								outObject.writeObject(false);
							} else {
								outObject.writeObject(true);
								bytesArray= new byte[(int)fileToSend.length()];
								FileInputStream fis = new FileInputStream(fileToSend);
								BufferedInputStream bis = new BufferedInputStream(fis);
								in = new DataInputStream(bis);
								in.readFully(bytesArray, 0, bytesArray.length);
								outObject.writeObject(bytesArray.length);
								outObject.write(bytesArray, 0, bytesArray.length);
								out.writeUTF("Le fichier " + args[1] + " a bien ete telecharge.");
							}
							break;
							
						default:
							break;
					}
				}
				
			} catch(IOException e) {
				System.out.println("Error handling client# " + clientNumber + ": " + e);
			}
			finally {
				
				try {
					socket.close();
				}
				catch (IOException e) {
					System.out.println("Couldn't close a socket, what's going on ?");
				}
				System.out.println("Connection with client# " + clientNumber + " closed");
			}
		}
		
		/**
		 * Retourne si le repertoire/fichier existe
		 * @param directory
		 * @return Boolean
		 */
		public static boolean directoryExists(String directory) {
			if (directory.contains("..") || directory.contains("//")) {
				return false;
			}
			if(currentPath.charAt(currentPath.length()-1) != '/') {
				currentPath += '/';
			}
			File file = new File(currentPath + directory + '/');
			return file.exists();
		}
		
		/**
		 * Retourne si l'on veut acceder a un repertoire imbrique dans un autre/d'autres
		 * @param directory
		 * @return boolean
		 */
		public static boolean areMultipleDirectories(String directory) {
			String[] directories = directory.split("/");
			return (directories.length > 1 && directory.contains("/"));
		}
	}

}




